﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParseLib
{
    /// <summary>
    /// 发票
    /// </summary>
    public class Invoice
    {
        /// <summary>
        /// 发票头
        /// </summary>
        public InvoiceHeader Header { get; set; }

        /// <summary>
        /// 发票明细
        /// </summary>
        public IList<InvoiceDetail> Details { get; set; }
    }
}
